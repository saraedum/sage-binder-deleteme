FROM registry.gitlab.com/sagemath/dev/sage/sagemath:binder
COPY --chown=sage:sage . .
