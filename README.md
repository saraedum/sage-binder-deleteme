# Generated Binder Configuration

Automatically generated [binder](https://mybinder.org) configuration for a [commit]({CI_PROJECT_URL}/commit/28ff169326f719aad0a5b036dd70d0c58187fd99) made on `Thu, 3 May 2018 00:13:32 +0200` about which the author said:
```
Try to make escapeJson more POSIX compliant
```
You can experiment with this commit by clicking here:
[![Binder](https://mybinder.org/badge.svg)](https://mybinder.org/v2/gl/saraedum%2Fsage-binder-deleteme/binder?filepath=review.ipynb)
